
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Media Offine</h1>
    <h3>Sosial Media Developer</h3>
    <p>Belajar dan berbagi agar hidup menjadi berkah</p>

    <h3>Keuntungan join di Media Offline</h3>
    <ul>
        <li>Mendapatkan Motivasi dari sesama para Developer</li>
        <li>Sharing Knowledge</li>
        <li>Dibuat oleh calon developer terbaik</li>
    </ul>
    
    <h3>Cara gabung ke Media Offline</h3>
    <ol>
        <li>Mengunjungi Website ini</li>
        <li>Mendaftarkan di <a href="{{url('/register')}}"><strong>Form Pendaftaran</strong></a>
        <li>Selesai</li>
    </ol>
    
</body>
</html>